﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class CameraManager : MonoBehaviour
{

	void Start ()
    {
        SetCameraEffects();
	}

    public void SetCameraEffects()
    {
        if (PlayerPrefs.HasKey("AA"))
        {
            if(PlayerPrefs.GetInt("AA") == 0)
            {
                gameObject.GetComponent<Antialiasing>().enabled = false;
            }
            else
            {
                gameObject.GetComponent<Antialiasing>().enabled = true;
            }
            
        }

        if (PlayerPrefs.HasKey("Bloom"))
        {
            if (PlayerPrefs.GetInt("Bloom") == 0)
            {
                gameObject.GetComponent<BloomAndFlares>().enabled = false;
            }
            else
            {
                gameObject.GetComponent<BloomAndFlares>().enabled = true;
            }

        }

        if (PlayerPrefs.HasKey("SSAO"))
        {
            if (PlayerPrefs.GetInt("SSAO") == 0)
            {
                gameObject.GetComponent<ScreenSpaceAmbientOcclusion>().enabled = false;
            }
            else
            {
                gameObject.GetComponent<ScreenSpaceAmbientOcclusion>().enabled = true;
            }

        }

    }
}
