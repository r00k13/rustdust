﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFlamethrower : Gun
{
    public GameObject flames;

    public override void Fire()
    {
        if (!fireSound.isPlaying)
        {
            fireSound.Play();
        }

        flames.SetActive(true);
        Invoke("StopFiring", 2f);
    }

    void StopFiring()
    {
        flames.SetActive(false);
    }
}
