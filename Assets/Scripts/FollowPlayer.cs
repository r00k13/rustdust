﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour 
{
	public int distanceX;
	public int distanceY;
	public int distanceZ;
	public bool following = true;
	
	public GameObject player;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(following)
		{
			Vector3 location = new Vector3(player.transform.position.x + distanceX, player.transform.position.y + distanceY, player.transform.position.z + distanceZ);
			transform.position = location;
		}
	}
}
