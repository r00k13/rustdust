﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuManager : MonoBehaviour
{
    public GameObject canvasMenu;
    public GameObject canvasOptions;
    public GameObject canvasAbout;
    public GameObject canvasLoad;

    private AudioSource click;
    
    public Button defaultButtonMenu;
    public Button defaultButtonOptions;
    public Button defaultButtonAbout;

    public Toggle toggleAA;
    public Toggle toggleBloom;
    public Toggle toggleSSAO;

    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        click = gameObject.GetComponent<AudioSource>();
		Time.timeScale = 1.0f;

        canvasLoad.SetActive(false);

        if (PlayerPrefs.GetInt("AA") == 1)
        {
            toggleAA.isOn = true;
        }
        if (PlayerPrefs.GetInt("Bloom") == 1)
        {
            toggleBloom.isOn = true;
        }
        if (PlayerPrefs.GetInt("SSAO") == 1)
        {
            toggleSSAO.isOn = true;
        }

        defaultButtonMenu.Select();
    }

    public void ShowMenu()
    {
        if (click != null)
        {
            click.Play();
        }

        canvasMenu.SetActive(true);
        canvasOptions.SetActive(false);
        canvasAbout.SetActive(false);

        defaultButtonMenu.Select();
    }

    public void Play()
    {
        if(click != null)
        {
            click.Play();
        }
        canvasLoad.SetActive(true);
        SceneManager.LoadScene(4);
    }

    public void PlayEndless()
    {
        if (click != null)
        {
            click.Play();
        }
        canvasLoad.SetActive(true);
        SceneManager.LoadScene(2);
    }

    public void PlayTutorial()
    {
        if (click != null)
        {
            click.Play();
        }
        canvasLoad.SetActive(true);
        SceneManager.LoadScene(1);
    }

    public void Options()
    {
        if (click != null)
        {
            click.Play();
        }
        canvasMenu.SetActive(false);
        canvasOptions.SetActive(true);

        defaultButtonOptions.Select();
    }

    public void ConfirmOptions()
    {
        if(toggleAA.isOn)
        {
            PlayerPrefs.SetInt("AA", 1);
        }
        else
        {
            PlayerPrefs.SetInt("AA", 0);
        }

        if (toggleBloom.isOn)
        {
            PlayerPrefs.SetInt("Bloom", 1);
        }
        else
        {
            PlayerPrefs.SetInt("Bloom", 0);
        }

        if (toggleSSAO.isOn)
        {
            PlayerPrefs.SetInt("SSAO", 1);
        }
        else
        {
            PlayerPrefs.SetInt("SSAO", 0);
        }

        gameObject.GetComponent<CameraManager>().SetCameraEffects();

        canvasMenu.SetActive(true);
        canvasOptions.SetActive(false);

        defaultButtonMenu.Select();
    }

    public void About()
    {
        if (click != null)
        {
            click.Play();
        }
        canvasMenu.SetActive(false);
        canvasAbout.SetActive(true);

        defaultButtonAbout.Select();
    }

    public void Quit()
    {
        if (click != null)
        {
            click.Play();
        }
        Application.Quit();
    }
}
