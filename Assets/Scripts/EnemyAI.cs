﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyAI : MonoBehaviour
{
    public Material friendlyMat;
    public Image mapIcon;
    public GameObject dustTrail;
    public GameObject allyLight;
    public Animator mesh;
    public Gun gun;

    public void Convert()
    {
        allyLight.GetComponent<Renderer>().material = friendlyMat;
        dustTrail.SetActive(false);
        GetComponent<EnemyMove>().healthBar.enabled = false;
        GetComponent<EnemyMove>().enabled = false;
        StartCoroutine(StartDefending());
        mesh.SetBool("converted", true);
        gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
        gameObject.GetComponent<UnityEngine.AI.NavMeshObstacle>().enabled = true;
        gameObject.tag = "Tower";

        if(mapIcon != null)
        {
            mapIcon.color = friendlyMat.color;
        }
    }

    IEnumerator StartDefending()
    {
        yield return new WaitForSeconds(0.75f);
        GetComponent<EnemyDefend>().enabled = true;
        gun.enabled = true;
    }
}
