﻿using UnityEngine;
using System.Collections;

public class GunBullet : Gun
{
    public GameObject bullet;
    public bool seeker = false;

    public override void Fire()
    {
        if(!fireSound.isPlaying)
        {
            fireSound.Play();
        }
        GameObject obj = Instantiate(bullet, transform.position + transform.forward, transform.rotation) as GameObject;
        obj.name = "Bullet";
        //obj.GetComponent<Projectile>().target = target.transform.position;
        //obj.transform.LookAt(new Vector3(target.transform.position.x, obj.transform.position.y, target.transform.position.z));
    }
}
