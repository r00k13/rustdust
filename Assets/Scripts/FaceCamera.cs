﻿using UnityEngine;
using System.Collections;

public class FaceCamera : MonoBehaviour
{
    private GameObject cam;

    // Use this for initialization
    void Start()
    {
        // find the player's camera
        cam = Camera.main.gameObject;
    }

    void Update()
    {
        this.gameObject.transform.LookAt(cam.transform.position, cam.transform.up);
        this.transform.Rotate(new Vector3(0, 180, 0));
    }
}
