﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;
    public Gun gun;
    public GameObject gunBarrel;
    public GameObject hackTool;
	public GameObject hackParticle;
    //public int maxBullets = 100;
    //public int currentBullets = 0;
    //private bool overheat = false;

    public int energy = 0;
    public GameObject[] energyBar;

    private LineRenderer line;

    //Ray ray;                                                        //ray from mouse location to point in 3D space
    //RaycastHit hit;

    //private AudioSource gunSound;
    private AudioSource pickupSound;

    // Use this for initialization
    void Start()
    {
        line = gameObject.GetComponent<LineRenderer>();
        line.enabled = false;

        //gunSound = gun.gameObject.GetComponent<AudioSource>();
        pickupSound = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //ray = new Ray(transform.position, transform.forward);
        //Camera.main.ScreenPointToRay(Input.mousePosition);    //sets rays position

        for (int i = 0; i < energyBar.Length; i++)                      //display as many segments of the energy bar as the player has energy
        {
            if(energy >= (i + 1))
            {
                energyBar[i].SetActive(true);
            }
            else
            {
                energyBar[i].SetActive(false);
            }
        }

        if(Time.timeScale == 1)
        {
            if (Input.GetButton("Fire1") || Input.GetAxis("TriggerRight") > 0.3)                                   //if fire button held
            {
                gun.Fire();
                //if (!gunSound.isPlaying)
                //{
                //    gunSound.Play();
                //}
            }

            if (Input.GetButtonUp("Fire1") || (Input.GetAxis("TriggerRight") < 0.3 && !Input.GetButton("Fire1")))
            {
                StopGun();
            }

            if (Input.GetButtonDown("Fire2") || Input.GetAxis("TriggerLeft") > 0.3)                             //if rmb held
            {
                StartCoroutine("FireBeam");
            }
        }

        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    energy += 3;
        //}

        //if (Physics.Raycast(ray, out hit))                  //if ray hits something 
        //{
        //    Vector3 targetPosition = new Vector3(hit.point.x, this.transform.position.y, hit.point.z);
        //    this.transform.LookAt(targetPosition);

        //    gun.transform.LookAt(hit.point);
        //    gun.transform.Rotate(Vector3.left, 90f);
        //    float gunRotation = gun.transform.localEulerAngles.x;
        //    gunRotation = Mathf.Clamp(gunRotation, 250, 300);
        //    gun.transform.localEulerAngles = new Vector3(gunRotation, gun.transform.localEulerAngles.y, gun.transform.localEulerAngles.z);


        //}


        //if (currentBullets == maxBullets)
        //{
        //    overheat = true;
        //}

        //if (overheat)
        //{
        //    Debug.Log("Overheat!");
        //    if (currentBullets == 0)
        //    {
        //        overheat = false;
        //    }
        //}
    }

    public void StopGun()
    {
        gun.CeaseFiring();
        //gunSound.Stop();
    }

    public int GetEnergy()
    {
        return energy;
    }

    public IEnumerator FireBeam()
    {
        line.SetPosition(0, hackTool.transform.position);
        line.SetPosition(1, hackTool.transform.position);

        Ray beam = new Ray(hackTool.transform.position, hackTool.transform.forward);                                                        //ray from mouse location to point in 3D space
        RaycastHit beamHit;

        if (Physics.Raycast(beam, out beamHit, 100f))                    //if ray hits something 
        {
            if (beamHit.collider.gameObject.tag == "Enemy" && energy > 2)
            {
                energy -= 3;
                line.SetPosition(1, beamHit.collider.gameObject.transform.position);
				Instantiate(hackParticle, beamHit.point, hackParticle.transform.rotation);

                if (beamHit.collider.gameObject.GetComponent<EnemyAI>() != null)
                {
                    beamHit.collider.gameObject.GetComponent<EnemyAI>().Convert();
                }
            }
            else if(beamHit.collider.gameObject.tag == "Pickup")
            {
                energy++;
                pickupSound.Play();
                line.SetPosition(1, beamHit.collider.gameObject.transform.position);
                Destroy(beamHit.collider.gameObject);
            }
            else
            {

            }

            line.enabled = true;
            yield return new WaitForSeconds(0.05f);
            line.enabled = false;
        }
    }

    void OnControllerColliderHit(ControllerColliderHit collision)
    {
        if (collision.gameObject.tag == "Pickup" && energy < 15)
        {
            energy++;
            pickupSound.Play();
            Destroy(collision.gameObject);
        }
    }
}
