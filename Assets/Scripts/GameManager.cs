﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public bool survival = true;

    public int maxHealth = 10;
    private int health;
    private static int score;
    public int targetScore = 25;

    public Image healthBar;

    public Text scoreText;

    public GameObject[] fires;

    public SpawnNPC[] spawnPoints;
    private int lastActivated = 0;

    public GameObject canvasWin;
    public GameObject canvasLose;
    public GameObject canvasMenu;
    public GameObject canvasControls;
    public GameObject canvasLoad;
    
    public Button defaultButtonMenu;
    public Button defaultButtonWin;
    public Button defaultButtonLose;
    public Button defaultButtonControls;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;

        score = 0;
        health = maxHealth;

        canvasLoad.SetActive(false);
        canvasMenu.SetActive(false);

        if (!survival)
        {
            canvasWin.SetActive(false);
            canvasLose.SetActive(false);
        }
    }

    void Update()
    {
        score = Mathf.FloorToInt(Time.timeSinceLevelLoad / 5);

        healthBar.fillAmount = (float)health / maxHealth;
        if(survival)
        {
            scoreText.text = " " + score;
        }
        else
        {
            scoreText.text = " " + score + "/" + targetScore;
        }

        //if (Input.GetKeyDown(KeyCode.Backspace))
        //{
        //    Restart();
        //}

        if (Input.GetButtonDown("Menu"))
        {
            ToggleMenu();
        }

        if (health <= 0 && Time.timeScale != 0)
        {
            Time.timeScale = 0;
            if (survival)
            {
                canvasLoad.SetActive(true);
                ScoreManager.score = score;
                SceneManager.LoadScene(3);
            }
            else
            {
                canvasLose.SetActive(true);
                defaultButtonLose.Select();
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }

        if(!survival && score >= targetScore && Time.timeScale != 0)
        {
            Time.timeScale = 0;
            canvasWin.SetActive(true);
            defaultButtonWin.Select();
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        if (lastActivated < spawnPoints.Length && Time.timeSinceLevelLoad >= 30 * lastActivated)
        {
            spawnPoints[lastActivated].activated = true;
            lastActivated++;
        }
    }

    public void ToggleMenu()
    {
        if(canvasMenu.activeInHierarchy)
        {
            Time.timeScale = 1;
            canvasMenu.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Time.timeScale = 0;
            canvasMenu.SetActive(true);
            canvasControls.SetActive(false);
            defaultButtonMenu.Select();
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    public void ToggleControls()
    {
        if (canvasControls.activeInHierarchy)
        {
            canvasControls.SetActive(false);
            canvasMenu.SetActive(true);
            defaultButtonMenu.Select();
        }
        else
        {
            canvasControls.SetActive(true);
            canvasMenu.SetActive(false);
            defaultButtonControls.Select();
        }
    }

    public void QuitToMenu()
    {
        canvasLoad.SetActive(true);
        SceneManager.LoadScene(0);
    }

    public void Restart()
    {
        Time.timeScale = 1;
        canvasLoad.SetActive(true);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ReceiveHit()
    {
        health--;

        if (health <= 2)
        {
            fires[2].SetActive(true);
        }
        else if (health <= 4)
        {
            fires[1].SetActive(true);
        }
        else if (health <= 6)
        {
            fires[0].SetActive(true);
        }
    }
}
