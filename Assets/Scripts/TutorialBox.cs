﻿using UnityEngine;
using System.Collections;

public class TutorialBox : MonoBehaviour
{
    public TutorialManager tutMgr;
    public int boxNo;
    public GameObject[] objects;

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            tutMgr.tutStage = boxNo;

            for(int i = 0; i < objects.Length; i++)
            {
                if (objects[i] != null)
                {
                    objects[i].SetActive(true);
                }
            }
        }
        
    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            for (int i = 0; i < objects.Length; i++)
            {
                if(objects[i] != null)
                {
                    objects[i].SetActive(false);
                }
            }
        }

    }
}
