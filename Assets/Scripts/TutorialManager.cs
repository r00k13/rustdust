﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TutorialManager : MonoBehaviour
{
    public int tutStage = 0;

    public GameObject health;
    public GameObject energy;
    public GameObject score;

    public GameObject healthMarker;
    public GameObject energyMarker;
    public GameObject scoreMarker;

    public GameObject canvasLoad;

    void Start()
    {
        canvasLoad.SetActive(false);
    }

    void Update ()
    {
        if(tutStage == 4)
        {
            health.SetActive(true);
            healthMarker.SetActive(true);
        }
        else
        {
            healthMarker.SetActive(false);
        }

        if (tutStage == 6)
        {
            energy.SetActive(true);
            energyMarker.SetActive(true);
        }
        else
        {
            energyMarker.SetActive(false);
        }

        if (tutStage == 10)
        {
            score.SetActive(true);
            scoreMarker.SetActive(true);
        }
        else
        {
            scoreMarker.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            canvasLoad.SetActive(true);
            SceneManager.LoadScene(5);
        }
    }
}
