﻿using UnityEngine;
using System.Collections;

public class CameraZoomFollow : MonoBehaviour
{
    public float distance;
    public float distanceMin;
    public float distanceMax;
    private float rotation = 0.0f;

    private float xSpeed = 150.0f;
    private float ySpeed = 90.0f;

    private float yMinLimit = 50f;
    private float yMaxLimit = 85f;

    private float x = 0.0f;
    private float y = 0.0f;

    public float zoomSpeed = 50f;

    public Transform player;                                                           //whatever the camera is following

    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;
    }

    void Update()
    {
        if (Time.timeScale == 1)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0 && distance < distanceMax)           //if scrollwheel moves, increase or decrease distance value
            {
                distance += Time.deltaTime * zoomSpeed;
            }
            else if (Input.GetAxis("Mouse ScrollWheel") > 0 && distance > distanceMin)
            {
                distance -= Time.deltaTime * zoomSpeed;
            }

            x += Input.GetAxis("Camera X") * xSpeed * 0.02f;
            y -= Input.GetAxis("Camera Y") * ySpeed * 0.02f;

            y = ClampAngle(y, yMinLimit, yMaxLimit);
        }

        Quaternion rotation = Quaternion.Euler(/*y*/60, x, 0);
        Vector3 position = new Vector3(0.0f, 0.0f, -distance);
        position = rotation * position;
        position += player.position;

        transform.rotation = rotation;
        player.rotation = Quaternion.Euler(0, x, 0);
        transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * 6f);

        transform.LookAt(player.transform);                                             //make camera look directly at player
    }

    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
        {
            angle += 360;
        }
        if (angle > 360)
        {
            angle -= 360;
        }
        return Mathf.Clamp(angle, min, max);
    }
}