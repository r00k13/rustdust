﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour 
{
	private Vector3 dir;
	public float speed;
	private CharacterController control;
    public Animator animate;
    public AudioSource walkSound;

    public GameObject hips;
    private Vector3 hipsRot;

    private bool stunned = false;
    public float stunDuration = 3.0f;
    public GameObject stunParticles;
    private float stunEnd = 0;
    
    // Use this for initialization
    void Start () 
	{
		dir = Vector3.zero;
		control = gameObject.GetComponent<CharacterController>();
        stunParticles.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () 
	{
        if (!stunned)
        {
            dir = transform.forward * Input.GetAxis("Vertical") + transform.right * Input.GetAxis("Horizontal");
            control.SimpleMove(dir * speed);

            if ((Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) && Time.timeScale != 0)
            {
                animate.SetBool("Moving", true);
                if(!walkSound.isPlaying)
                {
                    walkSound.Play();
                }
                hips.transform.LookAt(hips.transform.position + dir);
            }
            else
            {
                animate.SetBool("Moving", false);
                walkSound.Stop();
                hips.transform.eulerAngles = hipsRot;
            }
            hipsRot = hips.transform.eulerAngles;
        }
        else
        {
            if (Time.timeSinceLevelLoad > stunEnd)
            {
                stunned = false;
                gameObject.GetComponent<Shoot>().enabled = true;
                stunParticles.SetActive(false);
            }
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            EnemyMove em = collision.gameObject.GetComponent<EnemyMove>();
            if (em != null)
            {
                em.SelfDestruct();
                stunned = true;
                stunParticles.SetActive(true);
                animate.SetBool("Moving", false);
                gameObject.GetComponent<Shoot>().StopGun();
                gameObject.GetComponent<Shoot>().enabled = false;
                stunEnd = Time.timeSinceLevelLoad + stunDuration;
            }
        }
    }
}
