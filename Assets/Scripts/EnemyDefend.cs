﻿using UnityEngine;
using System.Collections;

public class EnemyDefend : MonoBehaviour
{
    public GameObject turret;
    public Gun gun;
    private GameObject target = null;

    Ray ray;
    RaycastHit hit;

    public float fireRate;
    public float range = 3;
    private float lastFired = 0.0f;

    // Update is called once per frame
    void Update ()
    {
        if(target == null)
        {
            turret.transform.Rotate(Vector3.up, 150f * Time.deltaTime);
        }
        else
        {
            turret.transform.LookAt(new Vector3(target.transform.position.x, turret.transform.position.y, target.transform.position.z));
            //turret.transform.Rotate(Vector3.left, 90f);
            if(Vector3.Distance(transform.position, target.transform.position) > range + 1.0f || target.GetComponent<EnemyMove>().enabled == false)
            {
                //turret.transform.localEulerAngles = new Vector3(0, 0, 0);
                target = null;
            }
        }
        

        if (Time.timeSinceLevelLoad > lastFired + fireRate)
        {
            ray = new Ray(gun.gameObject.transform.position, gun.gameObject.transform.forward);

            if (Physics.Raycast(ray, out hit, range))                    //if ray hits something 
            {
                Debug.Log(hit.collider.gameObject.tag);
                if (hit.collider.gameObject.tag == "Enemy")
                {
                    target = hit.collider.gameObject;
                    gun.Fire();
                    lastFired = Time.timeSinceLevelLoad;
                }
            }
        }
    }
}
