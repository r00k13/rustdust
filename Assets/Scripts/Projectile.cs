﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public Vector3 target;
    public float damage;
    public int speed;
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
	}

    void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag != "Tower")
        {
            Destroy(gameObject);
        }
        
    }
}
