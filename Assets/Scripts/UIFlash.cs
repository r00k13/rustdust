﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIFlash : MonoBehaviour
{
    private Outline outline;
    public float flashRate = 0.5f;
    private float lastFlash = 0;

	// Use this for initialization
	void Start ()
    {
        outline = gameObject.GetComponent<Outline>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(Time.timeSinceLevelLoad > lastFlash + flashRate)
        {
            outline.enabled = !outline.isActiveAndEnabled;
        }
	}
}
