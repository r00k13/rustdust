﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyMove : MonoBehaviour {

    //private CharacterController control;
    private GameObject target;
    private GameManager gm;

    public GameObject pickup;
    public GameObject explosion;
    public Image healthBar;

    private float health;
    public float healthMax = 3;

    public bool targetTowers = false;

    // Use this for initialization
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("Base").GetComponent<GameManager>();

        health = healthMax;
        healthBar.enabled = false;

        PickTarget();

        //Vector3 targetPostition = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
        //this.transform.LookAt(targetPostition);
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.fillAmount = (float)health / healthMax;
        
        if(target == null)
        {
            PickTarget();
        }
        else
        {
            //if (targetTowers)
            //Debug.Log(Vector3.Distance(transform.position, target.transform.position));
        }

        if (targetTowers && target != GameObject.FindGameObjectWithTag("Base") && Vector3.Distance(transform.position, target.transform.position) < 5.0f)
        {
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
            Destroy(target);
        }
    }

    void PickTarget()
    {
        target = GameObject.FindGameObjectWithTag("Base");
        UnityEngine.AI.NavMeshAgent agent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();

        if (targetTowers && GameObject.FindGameObjectWithTag("Tower") != null)
        {
            target = GameObject.FindGameObjectsWithTag("Tower")[0];
            
            for(int i = 0; i < GameObject.FindGameObjectsWithTag("Tower").Length; i++)
            {
                if(Vector3.Distance(transform.position, target.transform.position) > Vector3.Distance(transform.position, GameObject.FindGameObjectsWithTag("Tower")[i].transform.position))
                {
                    target = GameObject.FindGameObjectsWithTag("Tower")[i];
                }
            }
        }
        agent.SetDestination(target.transform.position);
    }

    public void AttackBase()
    {
        gm.ReceiveHit();
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    public void ReceiveHit(float dmg)
    {
        health -= dmg;
        healthBar.enabled = true;
        if (health <= 0)
        {
            Instantiate(pickup, transform.position + new Vector3(0, 0.5f, 0), transform.rotation);
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }

    public void SelfDestruct()
    {
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider collision)
    {
        //Debug.Log("Collide with " + collision.gameObject.name);
        if (collision.gameObject.tag == "Base")
        {
            AttackBase();
        }

        if (collision.gameObject.tag == "Bullet" && enabled)
        {
            ReceiveHit(collision.gameObject.GetComponent<Projectile>().damage);
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.tag == "Fire")
        {
            ReceiveHit(Time.deltaTime * 2);
        }
    }
}
