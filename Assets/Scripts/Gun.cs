﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour
{
    public float damage;
    protected AudioSource fireSound;

    // Use this for initialization
    void Start ()
    {
        enabled = false;
        fireSound = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        

    }

    public virtual void Fire()
    {

    }

    public virtual void CeaseFiring()
    {

    }
}
