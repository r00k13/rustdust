﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ScoreManager : MonoBehaviour 
{
    public static int score = 0;

    public Text[] scoreText;
    public InputField input;

    public GameObject canvasSubmit;
    public GameObject canvasScores;
    public GameObject canvasLoad;

    public Button defaultButtonSubmit;
    public Button defaultButtonMenu;

    private AudioSource click;

    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        click = gameObject.GetComponent<AudioSource>();
		Time.timeScale = 1.0f;

        canvasLoad.SetActive(false);
        defaultButtonSubmit.Select();
    }

    void Update()
    {
        if (Input.GetButtonDown("Menu"))
        {
            Menu();
        }
    }

    public void Menu()
    {
        if (click != null)
        {
            click.Play();
        }
        canvasLoad.SetActive(true);
        SceneManager.LoadScene(0);
    }

    public void SubmitScores()
    {
        if (click != null)
        {
            click.Play();
        }
        SetHiScore(score, input.text);
    }

    void SetHiScore(int myScore, string playerName)
    {
        if(playerName == "DELETEALL")
        {
            PlayerPrefs.DeleteAll();
        }
        else
        {
            string scoreKey = "HScore";
            string scoreNameKey = "HScoreName";

            int newScore = myScore;
            string newName = playerName;
            int oldScore;
            string oldName;

            for (int i = 0; i < scoreText.Length; i++)
            {
                if (PlayerPrefs.HasKey(scoreKey + i))
                {
                    if (PlayerPrefs.GetInt(scoreKey + i) < newScore)
                    {
                        oldScore = PlayerPrefs.GetInt(scoreKey + i);
                        oldName = PlayerPrefs.GetString(scoreNameKey + i);

                        PlayerPrefs.SetInt(scoreKey + i, newScore);
                        PlayerPrefs.SetString(scoreNameKey + i, newName);
                        newScore = oldScore;
                        newName = oldName;
                    }
                }
                else
                {
                    PlayerPrefs.SetInt(scoreKey + i, newScore);
                    PlayerPrefs.SetString(scoreNameKey + i, newName);
                    newScore = 0;
                    newName = "";
                }
            }
        }
        
        ShowHiScores();
    }

    void ShowHiScores()
    {
        string scoreKey = "HScore";
        string scoreNameKey = "HScoreName";

        for (int i = 0; i < scoreText.Length; i++)
        {
            scoreText[i].text = PlayerPrefs.GetString(scoreNameKey + i) + ": " + PlayerPrefs.GetInt(scoreKey + i);
        }

        canvasSubmit.SetActive(false);
        canvasScores.SetActive(true);
        defaultButtonMenu.Select();
    }
}
