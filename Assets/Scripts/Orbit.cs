﻿using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour 
{
	public GameObject target;
	public int speed = 10;
	//private GameManager gm;
	
	// Use this for initialization
	void Start () 
	{
		//gm = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//if(gm.gameState == 2)
		//{
			transform.RotateAround(target.transform.position, target.transform.up, speed * Time.deltaTime);
		//}
	}
}
