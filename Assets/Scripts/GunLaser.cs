﻿using UnityEngine;
using System.Collections;

public class GunLaser : Gun
{
    private LineRenderer line;

    private Ray shotDetect;                 //for raycasting
    private RaycastHit hit;

    public GameObject muzzle;
    public GameObject impact;

    void Start()
    {
        line = gameObject.GetComponent<LineRenderer>();
        fireSound = gameObject.GetComponent<AudioSource>();
        CeaseFiring();
    }

    public override void Fire()
    {
        shotDetect = new Ray(transform.position, transform.forward);

        if (Physics.Raycast(shotDetect, out hit, 500f))                    //if ray hits a collider
        {
            line.SetPosition(0, transform.position);
            line.SetPosition(1, hit.point);

            if (!fireSound.isPlaying)
            {
                fireSound.Play();
            }

            muzzle.SetActive(true);
            impact.SetActive(true);

            impact.transform.position = hit.point;

            if (hit.collider.gameObject.tag == "Enemy")                     //if collider is attached to an enemy
            {
                hit.collider.gameObject.GetComponent<EnemyMove>().ReceiveHit(Time.deltaTime * damage);   //damage enemy
            }
        }
    }

    public override void CeaseFiring()
    {
        line.SetPosition(0, transform.position);
        line.SetPosition(1, transform.position);

        fireSound.Stop();
        muzzle.SetActive(false);
        impact.SetActive(false);
    }
}
