﻿using UnityEngine;
using System.Collections;

public class SpawnNPC : MonoBehaviour 
{
	public GameObject [] prefabs;
    public GameObject teleport;
	
	public bool activated = false;
    public bool filled = false;
	
	public float spawnRate = 5.0f;
	private float nextSpawn = 0f;

	void Update () 
	{
		if(activated && !filled)
		{
			if (Time.time > nextSpawn) 											//if current time is at least time a regen is due
			{
				nextSpawn = Time.time + spawnRate;								//set value of nextRegen to current time + time taken to regenerate

				createNPC (Random.Range(0,prefabs.Length), transform.position);
			}
		}
	}
	
	void createNPC(int type, Vector3 location)
	{
        Instantiate(teleport, transform.position, transform.rotation);
		GameObject obj = Instantiate (prefabs[type], location, prefabs[type].transform.rotation) as GameObject;	//instantiate the object
		obj.name = "Enemy";	
	}

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            filled = true;
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            filled = false;
        }
    }
}
